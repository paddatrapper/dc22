#!/bin/sh

for file in $(ls svg)
do
  inkscape "svg/$file" --export-type=png --export-filename="png/$file"
done
