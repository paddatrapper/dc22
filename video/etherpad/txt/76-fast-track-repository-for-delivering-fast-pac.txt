Welcome to DebConf Video Etherpad!

This pad text is synchronized as you type, so that everyone viewing this page sees the same text. This allows you to collaborate seamlessly on documents!

To get started click the 👥 button in the top/bottom right and log in.

Get involved with Etherpad at https://etherpad.org


You said fasttrack has packages from new.  Do you consider fasttrack rather a solution or a workaround for sometimes slow new processing?

