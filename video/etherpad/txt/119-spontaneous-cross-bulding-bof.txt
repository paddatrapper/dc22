Spontaneous Cross Building bof

Introduction
	* Build Architecture is what you build on
	* Host Architecture is what you build for
	* Target architecture is wrong unless you know its right
	* If build architecture == host architecture then native build else cross build
	* sbuild --host=somehostarchitecture
	* pbuilder --host-arch=somehostarchitecture
	* https://crossqa.debian.net/src/yoursourcepackage
	* salsa-ci supports cross builds (thanks to IOhannes!)
	* Support: mail debian-cross@lists.debian.org or irc.oftc.net #debian-bootstrap

Your Questions
	* With salsa-ci , I got some failure with test-crossbuild-arm64
		* https://salsa.debian.org/ruby-team/ruby-strptime/-/jobs/3016526/raw
			* The following packages have unmet dependencies:
			* builddeps:.:arm64 : Depends: gem2deb:arm64 but it is not going to be installed
			* Depends: bundler:arm64 but it is not installable
			* Depends: rake-compiler:arm64 but it is not installable
			* Depends: ruby-rspec:arm64 but it is not installable
			* Depends: yard:arm64 but it is not installable
		* Questions
			* Some of those packages are provided as Architecture: all, but tried to fetch arm64 arch and failed. Why are they doing so?
			* And some packages have arm64 arch package but also failed. How to avoid this faillre?
				* $ rmadison -a arm64 gem2deb
				* gem2deb    | 0.33.1        | oldoldstable       | arm64
				* gem2deb    | 0.37~bpo9+1   | stretch-backports  | arm64
				* gem2deb    | 0.43          | oldstable          | arm64
				* gem2deb    | 1.1~bpo10+1   | buster-backports   | arm64
				* gem2deb    | 1.4           | stable             | arm64
				* gem2deb    | 1.8~bpo11+1   | bullseye-backports | arm64
				* gem2deb    | 1.8           | testing            | arm64
				* gem2deb    | 1.8           | unstable           | arm64
	* is there an overview that points out which language eco-system should work (even with some tweaks) and which ones do not? like: packages written in C/C++ can be crossed, what about Rust, Go, Haskell... and what are the blocker bugs?
		*  python: libpythonX.Y-something-dev, pythonX.Y-something:native
	* Does changing arch:all to arch:any help (sometimes, often, never?)
	* What to do about gir (gobject introspection something) stuff?
		* https://wiki.debian.org/CrossBuildPackagingGuidelines#gobject-introspection
