Debian India BoF at DebConf 2022

AGENDA
===========
- Hello
- Strategies for the community activities post pandemic.
   - Revamping Free Software Camp
   - Localisation teams for various Indian languages
   - Events aimed at non-techinical folks.
- Precursor events before DebConf 23.
- Your wishlist for DebConf 23.
- 



=== People attending ===
1. Anupa
2. Abraham
3. Praveen
4. Sruthi
5. Abhijith PA
6. Pavit Kaur
7. Ravi
8. Raju
9. Akshat
10. Utkarsh


=== Notes ===

1 . Introduction by each member present

2. Community Activities post pandemic
		- Held Multiple events during the pandemic and pulled of some really good events. 
		
		- Online events are not so helpful in terms of building the community properly and having useful engagement. 
		
		- Organize smaller events in local areas across India. 
		
		- More local events like release parties, Debian Days, packaging workshops, etc. 
		
		- Website for Free Software camp India https://camp.fsci.in/
		
		- Utkarsh - Have bi-weekely calls online to keep community engaged. 
		
		
		
		
3. Locallisation teams for Various Indian Languages
        - Ravi - Promote Debian more in Indian languages. 
        - Pavit - awareness about resources on translation is needed.
        - Praveen - There are people/Team who you can reach out to for contributing to Localisation
        
4. Events aimed at Non-Technical Folks
       - Abraham - It is not easy for non-technical folks ( Musicians, Artists etc) to understand on how to contribute to Debian. 
       
      - Akshat - Have Debian themed artwork and events 
                     - Mentioned about Debian Stall at the National Science day in Pune
                     - Promote design tools in school and university students. 
      
      - Raju - Akshat - Abraham
         - How to make Debian more lucrative for non-technical Design / Artists.. 
         - Help the Debian Multimedia and blends team to release an ISO with multimedia tools and a installation medium which is easier to get started for non - tech folks. 
         
         
5. PreCursor events for DebConf 2023
	- Anupa - Would like to see some public campaigns happening around Debian. (Installation fests, Release Parties) 
	- Akshat - Have a stall for Debian India at the upcoming science day 2023 in Pune 
	- Praveen - Have Debian stalls in free software meetups, across India.
	- Sruthi - Along with the smaller events, we should have atleast 2 big events as precursor to DebConf 2023
	- Anupa - Any number of people coming to Debian Indian are welcome, although we might not be able to manage a large number of people of DebConf
	- Abraham - Have follow up events to keep the communities engaged even after the events. 
	- Praveen - Keep people engaged across social media. 
	
	
	
6. Wishlist for DebConf 2023
    - Abraham - like to see a lot of new people at DebConf 2023, let them have the opportunity to meet Debian Developers
    - Ravi - Good and diverse food from across India
    - Anupa - People submiting talks should inform 
    - Raju - Have a mundu with Debian swirl in swag
    - Pavit - Have more diverse talks from across India
    - Abhijit - Learn from the mistake, take care of accessibility, co-ordinate import of video equipment in India. 
    - Utkarsh - need soap in washrooms (Praveen replie's, its a hotel) 
    - Akshat - Have a washing Machine on site for Laundry araara
	
	
	
	
