# DebConf 22 - Kosovo

This repo contains the DebConf 22 Team's shared documents and resources.

## Accounting

The `budget` directory contains the budget and conference books. Some
invoices may be redacted / stored elsewhere if they contain
sensitive information.
